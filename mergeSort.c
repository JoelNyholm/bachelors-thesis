#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

int arrSize = 0;
int *dataSet;

void merge(int left, int middle, int right)
{
    int i, j;
    int k = left;
    int leftPos = middle - left + 1;
    int rightPos = right - middle;
    int *leftTemp = (int *) malloc(leftPos*sizeof(int));
    int *rightTemp = (int *) malloc(rightPos*sizeof(int));
    
    //Faster with static arrays but causes stack overflow when arrSize > 10^6
    //int leftTemp[leftPos], rightTemp[rightPos];
  
    for (i = 0; i < leftPos; i++)
        leftTemp[i] = dataSet[left + i];

    for (j = 0; j < rightPos; j++)
        rightTemp[j] = dataSet[middle + 1 + j];

    i = 0; 
    j = 0; 
    while (i < leftPos && j < rightPos) {
        if (leftTemp[i] <= rightTemp[j]) 
            dataSet[k++] = leftTemp[i++];
        else 
            dataSet[k++] = rightTemp[j++];
    }
 
    while (i < leftPos) 
        dataSet[k++] = leftTemp[i++];
    while (j < rightPos) 
        dataSet[k++] = rightTemp[j++];

    free(leftTemp);
    free(rightTemp);
}
 
void mergeSort(int left, int right)
{
    if (left < right) {
        int middle = (left + right) / 2;
        mergeSort(left, middle);
        mergeSort(middle + 1, right);
        merge(left, middle, right);
    }
}

void printArray()
{
    for (int i = 0; i < arrSize; i++)
        printf("%d ", dataSet[i]);
    printf("\n");
}

void checkArraySorted(){
    for(int i = 0; i < arrSize-1 ; i++) { 
        if(dataSet[i] > dataSet[i+1]) 
            {
                printf("Array is not sorted in assenting order\n");
            }
    }
    printf("Array is sorted in assenting order\n");
}

void initArray(char *argv[])
{
    FILE *file = NULL;
    if (strcmp(argv[1], "s") == 0) {
        arrSize = pow(10,6);
        file = fopen("dataSets/s.txt","r");
    }     

    if (strcmp(argv[1], "m") == 0){
        arrSize = 5 * pow(10,6);
        file = fopen("dataSets/m.txt","r");
    }

    if(strcmp(argv[1], "l") == 0){
        arrSize = pow(10,7);
        file = fopen("dataSets/l.txt","r");
    }

    if(file == NULL)
    {
        printf("Error!\n");   
        exit(1);             
    }

    dataSet = (int *) malloc(arrSize*sizeof(int));
    for (int i = 0; i < arrSize; i++){
       
       if(fscanf(file, "%d", &dataSet[i]) != 1){
            printf("Error!\n");  
            exit(1);             
       };
    }
    fclose(file);
}


int main(int argc, char **argv)
{
    struct timespec start, end;
    double timeInSeconds;
    double timeInMilliSeconds = 0;

    initArray(argv);


    clock_gettime(CLOCK_MONOTONIC, &start);
    mergeSort(0, arrSize - 1);
    clock_gettime(CLOCK_MONOTONIC, &end);

    timeInSeconds = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9;
    timeInMilliSeconds = timeInSeconds * pow(10,3);
    printf( "%lf\n", timeInMilliSeconds );
    free(dataSet);
    return 0;
}
