#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#define NR_OF_NODES (8)
#define HYPERCUBE_DIMENSIONS (3) //log2(NR_OF_NODES)

struct Node 
{ 
    int id;
    int group;
    int idBinary [HYPERCUBE_DIMENSIONS];
    int nodeSize;
    int nrLowerThanPivot;
    int emptySpaces;
    int* data;
};

int *dataBuffer[NR_OF_NODES];
int nodePivot [NR_OF_NODES/2];
int arrSize = 0;
int *dataSet;
int count = 0;
int lastNodePos = 0;
clock_t start, end;
pthread_barrier_t barrier;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t* cond = NULL;


struct Node nodes [NR_OF_NODES];

int partition (int arr[], int low, int high) 
{ 
    int pivot = arr[high];
    register int i = (low - 1);
    int temp;
  
    for (register int j = low; j <= high - 1; j++) 
    { 
        if (arr[j] < pivot) 
        { 
            i++;
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        } 
    } 
    temp = arr[i+1];
    arr[i+1] = arr[high];
    arr[high] = temp;
    return (i + 1); 
} 
  
void quickSort(int arr[], int low, int high) 
{ 
    if (low < high) 
    { 
        int pivot = partition(arr, low, high); 
        quickSort(arr, low, pivot - 1); 
        quickSort(arr, pivot + 1, high); 
    } 
} 

int partitionNode(int arr[], int low, int high, int pivotPos)
{
    register int i = (low - 1), j;
    int temp;
    for (j = low; j <= high; j++) 
    { 
        if (arr[j] <= nodePivot[pivotPos]) 
        { 
            i++;
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        } 
    } 
    return (i+1);
}

void sendToPartner(struct Node *node, int pos) {
    if(node->idBinary[pos] == 0) {
        int sendTo = (pos << 1) + node->id;
        if(pos == 0)
            sendTo++;
        
        dataBuffer[sendTo][0] = node->nodeSize - node->nrLowerThanPivot; 
        node->emptySpaces = dataBuffer[sendTo][0];

        if(node->emptySpaces != 0) 
            memcpy(&dataBuffer[sendTo][1], &node->data[node->nrLowerThanPivot], (node->nodeSize - node->nrLowerThanPivot) * sizeof(int));
        
    } else {
        int sendTo = ((pos << 1) - node->id) * -1;
        if(pos == 0)
            sendTo--;

        dataBuffer[sendTo][0] = node->nrLowerThanPivot; 
        node->emptySpaces = dataBuffer[sendTo][0];
        memcpy(&dataBuffer[sendTo][1], &node->data[0], node->nrLowerThanPivot * sizeof(int));
    }
}

void recvAndMerge(struct Node *node, int pos) {
    int nrToRecv = dataBuffer[node->id][0];
    int tempSize = node->nodeSize - node->emptySpaces + nrToRecv;
    
    if(tempSize > 0){
        register int i,j,k;
        int nrToCopy = node->nodeSize - node->emptySpaces;
        int leftTemp[nrToRecv];
        int rightTemp[nrToCopy];

        if(node->idBinary[pos] == 1) i = node->nrLowerThanPivot;
        else i = 0;
        memcpy(&rightTemp[0], &node->data[i], nrToCopy * sizeof(int));
        memcpy(&leftTemp[0], &dataBuffer[node->id][1], nrToRecv * sizeof(int));

        node->data = (int*) realloc(node->data, sizeof(int) * tempSize);
        node->nodeSize = tempSize;
        node->emptySpaces = 0;
        i = 0; 
        j = 0;
        k = 0;
        while (i < nrToRecv && j < nrToCopy) {
            if (leftTemp[i] <= rightTemp[j]) 
                node->data[k++] = leftTemp[i++];
            else 
                node->data[k++] = rightTemp[j++];
        }
        while (i < nrToRecv) 
            node->data[k++] = leftTemp[i++];
        while (j < nrToCopy) 
            node->data[k++] = rightTemp[j++];

    } else {
        free(node->data);
        node->data = NULL;
        node->nodeSize = 0;
    }
}

void pickPivot(struct Node* node, int k){
    int nrOfGroups = NR_OF_NODES / (1 << (HYPERCUBE_DIMENSIONS - k));
    node->group = node->id / (1 << (HYPERCUBE_DIMENSIONS - k));
    
    int temp[NR_OF_NODES >> 1];
    register int i;
    for (i = 0; i < nrOfGroups; i++)
    {
        if (nrOfGroups == 2) temp[i] = i << 2;
        else temp[i] = i << 1;

        if(node->id == temp[i] && node->nodeSize > 0 )
        {
            nodePivot[node->group] = node->data[node->nodeSize >> 1];
        }
    }
}

void printNode(struct Node *node) {
    pthread_mutex_lock(&lock);

    printf("Node %d:  ", node->id);
    for (int i = 0; i < node->nodeSize; i++)
        printf("%d ", node->data[i]);
    //printf("nrLowerThanPivot: %d     empty: %d    size: %d", node->nrLowerThanPivot, node->emptySpaces, node->nodeSize);
    //printf("   pivot: %d    %d \n", nodePivot[0], nodePivot[1]);
    //printf("  size: %d\n", node->nodeSize);

    pthread_mutex_unlock(&lock);
}

void hyperQuickSort(struct Node *node) {
    register int i, k = 0;
    for (i = HYPERCUBE_DIMENSIONS -1; i > -1; i--)
    {
        pickPivot(node, k++);
        pthread_barrier_wait (&barrier);
        node->nrLowerThanPivot = partitionNode(node->data, 0, node->nodeSize -1, node->group);
        sendToPartner(node, i);
        pthread_barrier_wait (&barrier);
        recvAndMerge(node,i);
    }
}

void printArray(void)
{
    for (int i = 0; i < arrSize; i++)
        printf("%d ", dataSet[i]);
    printf("\n");
}

void sendToDataset(struct Node* node){
    pthread_mutex_lock(&lock);
    if (node->id != count) {
        pthread_cond_wait(&cond[node->id], &lock);
    }

    memcpy(&dataSet[lastNodePos], &node->data[0], node->nodeSize * sizeof(int));
    lastNodePos += node->nodeSize;

    if (count < NR_OF_NODES - 1) {
        count++;
    }
    else {
        count = 0;
    }
    pthread_cond_signal(&cond[count]);
    pthread_mutex_unlock(&lock);
}

void initArray(char *argv[])
{
    FILE *file = NULL;
    if (strcmp(argv[1], "s") == 0) {
        arrSize = pow(10,6);
        file = fopen("dataSets/s.txt","r");
    }     

    if (strcmp(argv[1], "m") == 0){
        arrSize = 5 * pow(10,6);
        file = fopen("dataSets/m.txt","r");
    }

    if(strcmp(argv[1], "l") == 0){
        arrSize = pow(10,7);
        file = fopen("dataSets/l.txt","r");
    }

    if(file == NULL)
    {
        printf("Error!\n");   
        exit(1);             
    }

    dataSet = (int *) malloc(arrSize*sizeof(int));
    for (int i = 0; i < arrSize; i++){
       
       if(fscanf(file, "%d", &dataSet[i]) != 1){
            printf("Error!\n");  
            exit(1);             
       };
    }
    fclose(file);
}


void initHyperCube() {
    int counter;
    int elementsPerNode = arrSize/NR_OF_NODES;
    for (int i = 0; i < NR_OF_NODES; i++) {
        nodes[i].idBinary[0] = (i & 0x1)/1;
        nodes[i].idBinary[1] = (i & 0x2)/2;
        nodes[i].idBinary[2] = (i & 0x4)/4;
        nodes[i].id = i;
        nodes[i].emptySpaces = 0;
    }

    counter = 0;
    for (int i = 0; i < NR_OF_NODES; i++)
    {
        nodes[i].nodeSize = elementsPerNode;
        nodes[i].data = (int *) malloc(elementsPerNode*sizeof(int));
        dataBuffer[i] = (int *)malloc( elementsPerNode * sizeof(int));

        for (int j = 0; j < elementsPerNode; j++)
        {
            nodes[i].data[j] = dataSet[counter++];
        }   
    }
}

void* initThread(void * args) {
    struct Node* node = (struct Node*) args; 
    quickSort(node->data, 0, node->nodeSize - 1); 
    hyperQuickSort(node); 
    sendToDataset(node);     
    free(node->data);
    pthread_exit(NULL);
}

void run(){
    pthread_t *threads = malloc( NR_OF_NODES * sizeof(pthread_t));
	struct Node* node;

	for (int i = 0; i < NR_OF_NODES; i++) {
		node = malloc(sizeof(struct Node));
		node = &nodes[i];
        pthread_cond_init(&cond[i], NULL);
		pthread_create(&(threads[i]), NULL, initThread, (void *) node);
	}

    for (int i = 0; i < NR_OF_NODES; i++)
    {
        pthread_join(threads[i], NULL);
    }
    free(threads);
}

void checkArraySorted(){
    for(int i = 0; i < arrSize-1 ; i++) { 
        if(dataSet[i] > dataSet[i+1]) 
            {
                printf("Array is not sorted in assenting order\n");
            }
    }
    printf("Array is sorted in assenting order\n");
}

int main(int argc, char **argv)
{
    pthread_barrier_init (&barrier, NULL, NR_OF_NODES);
    pthread_mutex_init(&lock, NULL);
    cond = (pthread_cond_t*)malloc(sizeof(pthread_cond_t)* NR_OF_NODES);
    struct timespec start, end;
    double timeInSeconds = 0;
    double timeInMilliSeconds = 0;

    initArray(argv);

    clock_gettime(CLOCK_MONOTONIC, &start);
    initHyperCube();
    run();
    clock_gettime(CLOCK_MONOTONIC, &end);
    
    timeInSeconds = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9;
    timeInMilliSeconds = timeInSeconds * pow(10,3);
    printf( "%lf\n", timeInMilliSeconds );

    pthread_mutex_destroy(&lock);
    free(dataSet);
    free(cond);
    for(int i = 0; i < NR_OF_NODES; i++)
        free(dataBuffer[i]);

    return 0;
}