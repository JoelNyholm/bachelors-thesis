# Usage

To run the sorting algorithms use make and then:
- p for parallel merge sort
- m for mergeSort
- q for quickSort
- h for hyperquicksort

```bash
make h
```
Then choose which dataset that you would like to sort
- l for large 
- m for medium 
- s for small

```bash
./test l
```