GCC=gcc

m:
	gcc -O2 -o test mergeSort.c -std=c17 
	./test l

q:
	gcc -O2 -o test quickSort.c -std=c17 
	./test l

h:
	gcc -O2 -o test hyperQuickSort.c -lpthread -std=c17 
	./test l

p:
	gcc -O2 -g -o  test parallelMergeSort.c -lpthread -std=c17 -Wall -Wextra
	./test l

clean:
	rm test