#!/usr/bin/env bash
gcc -O2 -o test quickSort.c -std=c17 
for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe s >> ./result/quicksort_Small.txt
    echo >> ./result/quicksort_Small.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe m >> ./result/quicksort_Medium.txt
    echo >> ./result/quicksort_Medium.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe l >> ./result/quicksort_Large.txt
    echo >> ./result/quicksort_Large.txt 
done



gcc -O2 -o test mergeSort.c -std=c17 
for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe s >> ./result/mergesort_Small.txt
    echo >> ./result/mergesort_Small.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe m >> ./result/mergesort_Medium.txt
    echo >> ./result/mergesort_Medium.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe l >> ./result/mergesort_Large.txt
    echo >> ./result/mergesort_Large.txt 
done




gcc -O2 -o test hyperQuickSort.c -lpthread -std=c17 
for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe s >> ./result/hyperquicksort_Small.txt
    echo >> ./result/hyperquicksort_Small.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe m >> ./result/hyperquicksort_Medium.txt
    echo >> ./result/hyperquicksort_Medium.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe l >> ./result/hyperquicksort_Large.txt
    echo >> ./result/hyperquicksort_Large.txt 
done



gcc -O2 -o test parallelMergeSort.c -lpthread -std=c17 
for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe s >> ./result/parallelMergesort_Small.txt
    echo >> ./result/parallelMergesort_Small.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe m >> ./result/parallelMergesort_Medium.txt
    echo >> ./result/parallelMergesort_Medium.txt 
done

for i in {1..1000}
do
    start RamCleaner.vbs
    ./test.exe l >> ./result/parallelMergesort_Large.txt
    echo >> ./result/parallelMergesort_Large.txt 
done