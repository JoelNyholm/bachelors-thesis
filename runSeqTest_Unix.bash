#!/usr/bin/env bash
gcc -O2 -o test quickSort.c -std=c17 
for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test s >> ./result/quicksort_Small.txt
    echo >> ./result/quicksort_Small.txt 
done

for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test m >> ./result/quicksort_Medium.txt
    echo >> ./result/quicksort_Medium.txt 
done

for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test l >> ./result/quicksort_Large.txt
    echo >> ./result/quicksort_Large.txt 
done



gcc -O2 -o test mergeSort.c -std=c17 
for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test s >> ./result/mergesort_Small.txt
    echo >> ./result/mergesort_Small.txt 
done

for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test m >> ./result/mergesort_Medium.txt
    echo >> ./result/mergesort_Medium.txt 
done

for i in {1..1000}
do
    echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a 
    ./test l >> ./result/mergesort_Large.txt
    echo >> ./result/mergesort_Large.txt 
done