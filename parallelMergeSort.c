#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#define NR_OF_NODES (8)
#define ITERATIONS (3) //log2(NR_OF_NODES)

struct Node 
{ 
    int id;
    int group;
    int groupPosition;
    int nodeSize;
    int groupMin, groupMax;
    int localHistogramSize;
    int groupHistogramSize;
    int* data;
    int* localHistogram;
    int* groupHistogram;
};

int arrSize;
int count = 0;
int lastNodePos = 0;
int *dataSet;
int *boundaryValues [NR_OF_NODES / 2];
int *histogramBuffer[NR_OF_NODES];
int groupPosCheck [NR_OF_NODES/2] [NR_OF_NODES];

pthread_barrier_t barrier;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t* cond = NULL;

struct Node nodes [NR_OF_NODES];

void merge(int arr[], int left, int middle, int right)
{
    int i, j;
    int k = left;
    int leftPos = middle - left + 1;
    int rightPos = right - middle;
    int *leftTemp = (int *) malloc(leftPos*sizeof(int));
    int *rightTemp = (int *) malloc(rightPos*sizeof(int));


    for (i = 0; i < leftPos; i++)
        leftTemp[i] = arr[left + i];

    for (j = 0; j < rightPos; j++)
        rightTemp[j] = arr[middle + 1 + j];

    i = 0; 
    j = 0; 
    while (i < leftPos && j < rightPos) {
        if (leftTemp[i] <= rightTemp[j]) 
            arr[k++] = leftTemp[i++];
        else 
            arr[k++] = rightTemp[j++];
    }
 
    while (i < leftPos) 
        arr[k++] = leftTemp[i++];
    while (j < rightPos) 
        arr[k++] = rightTemp[j++];

    free(leftTemp);
    free(rightTemp);
}
 
void mergeSort(int arr[], int left, int right)
{
    if (left < right) {
        int middle = (left + right) / 2;
        mergeSort(arr, left, middle);
        mergeSort(arr, middle + 1, right);
        merge(arr, left, middle, right);
    }
}

void insertionSort(int arr[], int nrOfElements) 
{ 
    register int i, j, temp; 
    for (i = 1; i < nrOfElements; i++) { 
        temp = arr[i]; 
        j = i - 1; 
  
        while (j >= 0 && arr[j] > temp) { 
            arr[j + 1] = arr[j]; 
            j = j - 1; 
        } 
        arr[j + 1] = temp; 
    } 
} 

void printNode(struct Node *node) {
    pthread_mutex_lock(&lock);

    printf("Node %d:  ", node->id);
    for (int i = 0; i < node->nodeSize; i++)
        printf("%d ", node->data[i]);
    printf("  size: %d   histoSize: %d\n", node->nodeSize, node->localHistogramSize);
    printf("  group: %d  groupPos: %d  groupSize %d\n",node->group, node->groupPosition, node->groupHistogramSize);
    pthread_mutex_unlock(&lock);
}

void printLocalHistogram(struct Node* node) {
    pthread_mutex_lock(&lock);
    int minValue = node->data[0];
    printf("Node %d:  \n", node->id);

    for (int i = 0; i < node->localHistogramSize; i++)
    {
        printf("%d: %d \n", minValue++, node->localHistogram[i]);
    }

    printf("\n\n");
    pthread_mutex_unlock(&lock);
}

void printGroupHistogram(struct Node* node) {
    pthread_mutex_lock(&lock);
    int minValue = node->groupMin;
    printf("Node %d:  \n", node->id);

    for (int i = 0; i < node->groupHistogramSize; i++)
    {
        printf("temp[%d] = %d: %d \n",i, minValue++, node->groupHistogram[i]);
    }

    printf("\n\n");
    pthread_mutex_unlock(&lock);
}

void createHistogram(struct Node* node, int currentIteration){

    node->localHistogramSize = node->data[node->nodeSize - 1] - node->data[0] + 1;
    if(currentIteration > 1){
        free(node->localHistogram);
    }

    node->localHistogram = (int *) calloc(node->localHistogramSize, sizeof(int));
    register int i, pos;
    for(i = 0; i < node->nodeSize; i++)
    {
        pos = node->data[i] - node->data[0];
        node->localHistogram[pos] += 1;
    }
}

void sendBoundaryValues(struct Node* node , int currentIteration) {
    int sendToPos = ((node->id << 1) % (1 << (currentIteration + 1))); //id*2 % 2^iteration+1
    boundaryValues[node->group][sendToPos++] = node->data[0];
    boundaryValues[node->group][sendToPos] = node->data[node->nodeSize-1];
}

void recvBoundaryValue(struct Node* node, int currentIteration) {
    int nrOfNodesInGroup = NR_OF_NODES / (1 << (ITERATIONS - currentIteration));
    int maxValues [nrOfNodesInGroup];
    int minValues [nrOfNodesInGroup];
    register int i, j = 0;

    for (i = 0; i < nrOfNodesInGroup * 2; i++)
    {
        if(i % 2 == 0){
            minValues[j] = boundaryValues[node->group][i];
        }else{
            maxValues[j] = boundaryValues[node->group][i];
            j++;
        }
    }

    insertionSort(minValues,nrOfNodesInGroup);
    insertionSort(maxValues, nrOfNodesInGroup);

    for (i = 0; i < nrOfNodesInGroup; i++)
    {
        if(node->data[0] == minValues[i] && groupPosCheck[node->group][i] <= 0){
            node->groupPosition = i;
            groupPosCheck[node->group][i] = 1;
            break;
        }
    }

    node->groupMin = minValues[0];
    node->groupMax = maxValues[nrOfNodesInGroup-1];
    node->groupHistogramSize = maxValues[nrOfNodesInGroup-1] - minValues[0] + 1;

    if(currentIteration > 1){
        free(node->groupHistogram);
    }
    node->groupHistogram = (int *) calloc(node->groupHistogramSize, sizeof(int));
}

void sendHistogramToPartner(struct Node* node, int currentIteration){
    int sendToPos = (node->group << 1 * currentIteration) + node->groupPosition;
    if(histogramBuffer[sendToPos] != NULL){
        free(histogramBuffer[sendToPos]);
    }
    histogramBuffer[sendToPos] = (int *) calloc((2 + node->localHistogramSize), sizeof(int));
    histogramBuffer[sendToPos][0] = node->localHistogramSize;
    histogramBuffer[sendToPos][1] = node->data[0];
    memcpy(&histogramBuffer[sendToPos][2], &node->localHistogram[0], node->localHistogramSize * sizeof(int));
}

void recvAndComputeHistograms(struct Node* node , int currentIteration){
    int nrOfNodesInGroup = NR_OF_NODES / (1 << (ITERATIONS - currentIteration));
    int startPos = node->group * 2 * currentIteration;
    int sizeOfHistogram;
    int minValue;
    int pos;
    register int j = 0, i, k;
    for (i = startPos; i < nrOfNodesInGroup + startPos; i++)
    {
        j = 0;
        sizeOfHistogram = histogramBuffer[i][j++];
        minValue = histogramBuffer[i][j++];

        for (k = 0; k < sizeOfHistogram; k++)
        {
            pos = k + (minValue - node->groupMin);
            node->groupHistogram[pos] += histogramBuffer[i][j++];
        }        
    }
}

void divideGroupHistogram(struct Node* node, int currentIteration){
    int nrOfNodesInGroup = NR_OF_NODES / (1 << (ITERATIONS - currentIteration));
    int divideSize = (int) floor(node->groupHistogramSize/nrOfNodesInGroup);
    int remainder = node->groupHistogramSize % nrOfNodesInGroup;
    int dividePos = 0;

    for (int i = 0; i < remainder; i++){
        if (node->groupPosition == i )
            divideSize++;
    }

    dividePos = node->groupPosition * divideSize;

    if(node->groupPosition >= remainder)
        dividePos += remainder;

    int minValue = node->groupMin + dividePos;
    int value = 0;
    int counter = 0;
    int newSize = 0;
    int* temp = (int *) malloc( divideSize * sizeof(int));
    
    memcpy(&temp[0], &node->groupHistogram[dividePos], divideSize * sizeof(int));
    for (int i = 0; i < divideSize; i++)
    {
        if(temp[i] > 0){
            newSize += temp[i];
        }
    }

    free(node->data);
    node->data = (int *) calloc(newSize, sizeof(int));
    node->nodeSize = newSize;

    for (int i = 0; i < divideSize; i++)
    {
        if(temp[i] > 0){
            for (int j = 0; j < temp[i]; j++)
            {
                value = i + minValue;
                node->data[counter++] = value;
            }
        }
    }
    
    free(temp);
}

void resetValues(struct Node* node){
    if(node->groupPosition == 0){
        memset(groupPosCheck[node->group], 0, sizeof(groupPosCheck[node->group]));
    }
}

void parallelMergeSort(struct Node* node){
    mergeSort(node->data, 0, node->nodeSize - 1);
    register int i;
    
    for (i = 1; i < (ITERATIONS + 1); i++)
    {
        node->group = node->id / (1 << (i));
        createHistogram(node,i);
        sendBoundaryValues(node, i);
        pthread_barrier_wait (&barrier);

        recvBoundaryValue(node, i);
        sendHistogramToPartner(node, i);
        pthread_barrier_wait (&barrier);

        recvAndComputeHistograms(node, i);
        divideGroupHistogram(node, i);
        pthread_barrier_wait (&barrier);
        
        resetValues(node);
    }
}

void sendToDataset(struct Node* node){
    pthread_mutex_lock(&lock);
    if (node->groupPosition != count) {
        pthread_cond_wait(&cond[node->groupPosition], &lock);
    }

    memcpy(&dataSet[lastNodePos], &node->data[0], node->nodeSize * sizeof(int));
    lastNodePos += node->nodeSize;

    if (count < NR_OF_NODES - 1) {
        count++;
    }
    else {
        count = 0;
    }
    pthread_cond_signal(&cond[count]);
    pthread_mutex_unlock(&lock);
}

void* initThread(void * args) {
    struct Node* node = (struct Node*) args; 
    parallelMergeSort(node);
    sendToDataset(node);     
    free(node->data);
    free(node->groupHistogram);
    free(node->localHistogram);
    pthread_exit(NULL);
}

void run(){
    pthread_t *threads = malloc( NR_OF_NODES * sizeof(pthread_t));
	struct Node* node;
    register int i;
	for (i = 0; i < NR_OF_NODES; i++) {
		node = malloc(sizeof(struct Node));
		node = &nodes[i];
        pthread_cond_init(&cond[i], NULL);
		pthread_create(&(threads[i]), NULL, initThread, (void *) node);
	}

    for (i = 0; i < NR_OF_NODES; i++)
    {
        pthread_join(threads[i], NULL);
    }
    free(threads);
}

void initArray(char *argv[])
{
    FILE *file = NULL;
    if (strcmp(argv[1], "s") == 0) {
        arrSize = pow(10,6);
        file = fopen("dataSets/s.txt","r");
    }     

    if (strcmp(argv[1], "m") == 0){
        arrSize = 5 * pow(10,6);
        file = fopen("dataSets/m.txt","r");
    }

    if(strcmp(argv[1], "l") == 0){
        arrSize = pow(10,7);
        file = fopen("dataSets/l.txt","r");
    }

    if(file == NULL)
    {
        printf("Error!\n");   
        exit(1);             
    }

    dataSet = (int *) malloc(arrSize*sizeof(int));
    for (int i = 0; i < arrSize; i++){
       
       if(fscanf(file, "%d", &dataSet[i]) != 1){
            printf("Error!\n");  
            exit(1);             
       };
    }
    fclose(file);
}


void initNodes() {
    int counter = 0;
    int elementsPerNode = arrSize/NR_OF_NODES;
    register int i,j;

    for (i = 0; i < NR_OF_NODES >> 1; i++)
    {
        boundaryValues[i] = (int *) malloc(NR_OF_NODES << 1 * sizeof(int));
    }
    
    for (i = 0; i < NR_OF_NODES; i++)
    {
        nodes[i].id = i;
        nodes[i].group = 0;
        nodes[i].nodeSize = 0;
        nodes[i].groupMin = 0;
        nodes[i].groupMax = 0;
        nodes[i].localHistogramSize = 0;
        nodes[i].groupHistogramSize = 0;
        nodes[i].nodeSize = elementsPerNode;
        nodes[i].groupPosition = -1;
        nodes[i].data = (int *) malloc(elementsPerNode*sizeof(int));

        for (j = 0; j < elementsPerNode; j++)
        {
            nodes[i].data[j] = dataSet[counter++];
        }   
    }
}

void printArray(void)
{
    for (int i = 0; i < arrSize; i++)
        printf("%d ", dataSet[i]);
    printf("\n");
}

void checkArraySorted(){
    for(int i = 0; i < arrSize-1 ; i++) { 
        if(dataSet[i] > dataSet[i+1]) 
            {
                printf("Array is not sorted in assenting order\n");
            }
    }
    printf("Array is sorted in assenting order\n");
}

int main(int argc, char **argv)
{
    pthread_barrier_init (&barrier, NULL, NR_OF_NODES);
    pthread_mutex_init(&lock, NULL);
    cond = (pthread_cond_t*)malloc(sizeof(pthread_cond_t)* NR_OF_NODES);
    
    struct timespec start, end;
    double timeInSeconds = 0;
    double timeInMilliSeconds = 0;

    initArray(argv);
   
    clock_gettime(CLOCK_MONOTONIC, &start);
    initNodes();
    run();
    clock_gettime(CLOCK_MONOTONIC, &end);

    timeInSeconds = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9;
    timeInMilliSeconds = timeInSeconds * pow(10,3);
    printf( "%lf\n", timeInMilliSeconds );

    pthread_mutex_destroy(&lock);
    free(dataSet);
    free(cond);
    for(int i = 0; i < NR_OF_NODES; i++)
        free(histogramBuffer[i]);
    for (int i = 0; i < NR_OF_NODES/2; i++)
        free(boundaryValues[i]);

    return 0;
}