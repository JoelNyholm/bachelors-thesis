#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

int arrSize = 0;
int *dataSet;

int partition (int low, int high) 
{ 
    int pivot = dataSet[high];
    register int i = (low - 1);
    int temp;
  
    for (register int j = low; j <= high - 1; j++) 
    { 
        if (dataSet[j] < pivot) 
        { 
            i++;
            temp = dataSet[i];
            dataSet[i] = dataSet[j];
            dataSet[j] = temp;
        } 
    } 
    temp = dataSet[i+1];
    dataSet[i+1] = dataSet[high];
    dataSet[high] = temp;
    return (i + 1); 
} 
  
void quickSort(int low, int high) 
{ 
    if (low < high) 
    { 
        int pivot = partition(low, high); 
        quickSort(low, pivot - 1); 
        quickSort(pivot + 1, high); 
    } 
} 

void printArray(void)
{
    for (int i = 0; i < arrSize; i++)
        printf("%d ", dataSet[i]);
    printf("\n");
}

void checkArraySorted(){
    for(int i = 0; i < arrSize-1 ; i++) { 
        if(dataSet[i] > dataSet[i+1]) 
            {
                printf("Array is not sorted in assenting order\n");
            }
    }
    printf("Array is sorted in assenting order\n");
}

void initArray(char *argv[])
{
    FILE *file = NULL;
    if (strcmp(argv[1], "s") == 0) {
        arrSize = pow(10,6);
        file = fopen("dataSets/s.txt","r");
    }     

    if (strcmp(argv[1], "m") == 0){
        arrSize = 5 * pow(10,6);
        file = fopen("dataSets/m.txt","r");
    }

    if(strcmp(argv[1], "l") == 0){
        arrSize = pow(10,7);
        file = fopen("dataSets/l.txt","r");
    }

    if(file == NULL)
    {
        printf("Error!\n");   
        exit(1);             
    }

    dataSet = (int *) malloc(arrSize*sizeof(int));
    for (int i = 0; i < arrSize; i++){
       
       if(fscanf(file, "%d", &dataSet[i]) != 1){
            printf("Error!\n");  
            exit(1);             
       };
    }
    fclose(file);
}



int main(int argc, char **argv)
{
    double timeInSeconds = 0;
    double timeInMilliSeconds = 0;
    struct timespec start, end;


    initArray(argv);

    clock_gettime(CLOCK_MONOTONIC, &start);
    quickSort(0, arrSize-1);
    clock_gettime(CLOCK_MONOTONIC, &end);

    timeInSeconds = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9;
    timeInMilliSeconds = timeInSeconds * pow(10,3);
    printf( "%lf\n", timeInMilliSeconds );
    free(dataSet);
    return 0;
}